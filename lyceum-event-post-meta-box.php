<?php

/**
 * Plugin Name: Custom Post Meta Box for Lyceum Theatre
 * Plugin URI: http://sfpixels.co/lyceum-metabox-plugin
 * Description: We needed some custom post meta boxes so I wrote a plugin.
 * Version: 1.0.0
 * Author: Hal Borland --  Southern Fried Pixels
 * Author URI: http://sfpixels.co/lyceum-metabox-plugin
 * License: GPL2
 */

//enqueue styles and javascripts for plugin.
function lt_add_admin_style(){
	wp_enqueue_script( 'jquery');
    wp_enqueue_style('jquery-style', plugins_url('lyceum-event-post-meta-box/css/jquery-ui.css'));
    wp_enqueue_script( 'jquery-ui', plugins_url( 'lyceum-event-post-meta-box/js/jquery-ui.js') , 'jquery');
    wp_enqueue_script( 'lt-app', plugins_url( 'lyceum-event-post-meta-box/js/app.js') , 'jquery-ui');


}
add_action( 'admin_enqueue_scripts', 'lt_add_admin_style' );
$prefix = 'dbt_';
$meta_box = array(
    'id' => 'my-meta-box',
    'title' => 'Lyceum Event Settings',
    'page' => 'event',
    'context' => 'normal',
    'priority' => 'high',
    'fields' => array(
        array(
            'name' => 'Event Start Date',
            'desc' => 'Please set the Event Start Date. ',
            'id' => $prefix . 'StartDate',
            'type' => 'text',
            'std' => 'Start Date'
        ),
        array(
            'name' => 'Event End Date',
            'desc' => 'Please set the Event End Date. ',
            'id' => $prefix . 'EndDate',
            'type' => 'text',
            'std' => 'End Date'
        ),
        array(
            'name' => 'Event Rating',
            'desc' => 'Please set the Rating for this Event. ',
            'id' => $prefix . 'rating',
            'type' => 'text',
            'std' => 'PG 13'
        ),
        // array(
        //     'name' => 'Textarea',
        //     'desc' => 'Enter big text here',
        //     'id' => $prefix . 'textarea',
        //     'type' => 'textarea',
        //     'std' => 'Default value 2'
        // ),
        // array(
        //     'name' => 'Select box',
        //     'id' => $prefix . 'select',
        //     'type' => 'select',
        //     'options' => array('Option 1', 'Option 2', 'Option 3')
        // ),
        // array(
        //     'name' => 'Radio',
        //     'id' => $prefix . 'radio',
        //     'type' => 'radio',
        //     'options' => array(
        //         array('name' => 'Name 1', 'value' => 'Value 1'),
        //         array('name' => 'Name 2', 'value' => 'Value 2')
        //     )
        // ),
        // array(
        //     'name' => 'Checkbox',
        //     'id' => $prefix . 'checkbox',
        //     'type' => 'checkbox'
        // )
    )
);


// Callback function to show fields in meta box
function mytheme_show_box() {
    global $meta_box, $post;
    // Use nonce for verification
    echo '<input type="hidden" name="mytheme_meta_box_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
    echo '<table class="form-table">';
    foreach ($meta_box['fields'] as $field) {
        // get current post meta data
        $meta = get_post_meta($post->ID, $field['id'], true);
        echo '<tr>',
                '<th style="width:20%"><label for="', $field['id'], '">', $field['name'], '</label></th>',
                '<td>';
        switch ($field['type']) {
            case 'text':
                echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="30" style="width:97%" />', '<br />', $field['desc'];
                break;
            case 'textarea':
                echo '<textarea name="', $field['id'], '" id="', $field['id'], '" cols="60" rows="4" style="width:97%">', $meta ? $meta : $field['std'], '</textarea>', '<br />', $field['desc'];
                break;
            case 'select':
                echo '<select name="', $field['id'], '" id="', $field['id'], '">';
                foreach ($field['options'] as $option) {
                    echo '<option ', $meta == $option ? ' selected="selected"' : '', '>', $option, '</option>';
                }
                echo '</select>';
                break;
            case 'radio':
                foreach ($field['options'] as $option) {
                    echo '<input type="radio" name="', $field['id'], '" value="', $option['value'], '"', $meta == $option['value'] ? ' checked="checked"' : '', ' />', $option['name'];
                }
                break;
            case 'checkbox':
                echo '<input type="checkbox" name="', $field['id'], '" id="', $field['id'], '"', $meta ? ' checked="checked"' : '', ' />';
                break;
        }
        echo     '</td><td>',
            '</td></tr>';
    }
    echo '</table>';
}


add_action('save_post', 'mytheme_save_data');
// Save data from meta box
function mytheme_save_data($post_id) {
    global $meta_box;
    // verify nonce
    if (!wp_verify_nonce($_POST['mytheme_meta_box_nonce'], basename(__FILE__))) {
        return $post_id;
    }
    // check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }
    // check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }
    foreach ($meta_box['fields'] as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}

add_action('admin_menu', 'mytheme_add_box');
// Add meta box
function mytheme_add_box() {
    global $meta_box;
    add_meta_box($meta_box['id'], $meta_box['title'], 'mytheme_show_box', $meta_box['page'], $meta_box['context'], $meta_box['priority']);
}